package SchoolManagementApp;

import java.util.Scanner;

public class SchoolManagementApp {
    public static void main(String[] args) {

        // Create a predetermined number of students
        System.out.print("Enter number of students to enroll: ");
        Scanner in = new Scanner(System.in);
        int numOfStudents = in.nextInt();
        Student[] students = new Student[numOfStudents];

        // Create individual students
        for(int i = 0; i < numOfStudents; i++) {
            students[i] = new Student();
            students[i].enroll();
            students[i].payTuition();
        }

        // List all enrolled students
        for(int i = 0; i < numOfStudents; i++) {
            System.out.println("\n" + students[i].toString());
        }
    }
}
