package SchoolManagementApp;

import java.util.Scanner;

public class Student {

    private String firstName;
    private String lastName;
    private String studentID;
    private String currentYear;
    private String courses = "";
    private int tuitionBalance = 0;
    private static int coursePrice = 500;
    private static int id = 1000;


    public Student() {
        Scanner in = new Scanner(System.in);
        System.out.print("\nEnter student first name: ");
        this.firstName = in.nextLine();

        System.out.print("Enter student last name: ");
        this.lastName = in.nextLine();

        System.out.print("1. Freshman\n2. Sophomore\n3. Junior\n4. Senior\nEnter student year: ");
        this.currentYear = in.nextLine();

        setStudentID();
    }

    private void setStudentID() {
        id++;
        this.studentID = currentYear + " " + id;
    }

    public void enroll() {
        do {
            System.out.print("Enter course to enroll in (enter Q to quit) ");
            Scanner in = new Scanner(System.in);
            String course = in.nextLine();
            if(!course.equals("Q")) {
                courses = courses + "\n" + course;
                tuitionBalance = tuitionBalance + coursePrice;
            }
            else {
                break;
            }
        } while (1 != 0);
    }

    public void viewBalance() {
        System.out.println("Your balance is: $" + tuitionBalance);
    }

    public void payTuition() {
        viewBalance();
        System.out.print("Enter tuition payment: $");
        Scanner in =  new Scanner(System.in);
        int payment = in.nextInt();
        tuitionBalance = tuitionBalance - payment;
        System.out.println("Thank You for your payment of: $" + payment);
        viewBalance();
    }

    public String toString() {
        return "Name: " + firstName + " " + lastName +
                "\nCurrent Year: " + currentYear +
                "\nStudent ID: " + studentID +
                "\nCourses enrolled:" + courses +
                "\nBalance: $" + tuitionBalance;
    }
}
